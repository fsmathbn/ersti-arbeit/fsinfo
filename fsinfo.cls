\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fsinfo}



% ------------------------------------------------------------------------------
%   GENERAL SETTINGS
% ------------------------------------------------------------------------------

\LoadClass{scrbook}
% This class is the KOMA-Script equivalent of the "book" class.
% KOMA-Script is a bundle (classes + packages) with extensive layout options.

\RequirePackage{microtype}
% Improved micro-typography: character protrusion and font expansion are enabled by default.

\RequirePackage{xparse}
% Provides better macro defining.
% Contained in the LaTeX kernel since October 2020.

\RequirePackage{xstring}
% Basic string manipulation.

\RequirePackage{csquotes}
% Automatically enforce typographically correct quotation marks, respecting the current language set by "babel".
% Use \enquote{<text>} for a regular quotation. This command can be nested.

\RequirePackage{graphicx}
% Provides \includegraphics to include image files.

\RequirePackage{booktabs}
% Better looking tables.

\RequirePackage[
	dvipsnames,
	table
]{xcolor}
% For a more colourful life.

\PassOptionsToPackage{hyphens}{url}
% Pass option "hyphens" to the package url which is loaded internally in hyperref to allow url breaking after a hyphen.

\RequirePackage{hyperref}
% This package enables hyperlinks and configures PDF metadata.

\RequirePackage{unicode-math}
% This package allows to load custom fonts and change font features.

\RequirePackage[shortlabels]{enumitem}
% Allows for better configuration of list environments.
% The option "shortlabels" allows to configure labels like in the package "enumerate".



% ------------------------------------------------------------------------------
%   CLASS OPTIONS
% ------------------------------------------------------------------------------

%%%
%%% GENERAL SETTINGS
%%%

\KOMAoptions{
    paper     = a4,
    fontsize  = 10pt,
    twoside   = semi,
    headings  = openany,
    twocolumn = true,
    parskip   = half,
    DIV       = 12,
    toc       = chapterentrydotfill
}
% Set options of the KOMA-Script class.
% "paper=a4" sets the page size to DINA4.
% "fontsize=10pt" sets the font size to 10pt.
% "twoside=semi" enables two-sided printing with one-sided margins.
% "headings=openany" starts new chapters on either side.
% "twocolumn=true" enables a two-column layout.
% "parskip=half" sets inter-paragraph spacing to half a line (without indentation).
% "DIV=n" divides the page into n stripes (horizontally and vertically).
% "toc=chapterentrydotfill" fills all lines in the TOC with dots (including chapter entries).

%%%%%
%%% AUXILIARY COMMANDS
%%%%%

\newcommand{\currentchapter}{}

\let\oldchapter\chapter
\RenewDocumentCommand{\chapter}
  {s o m}
  {%
    \renewcommand{\currentchapter}{#3}%
    \IfBooleanTF{#1}%
    {%
      \oldchapter*{#3}%
    }%
    {%
      \IfValueTF{#2}%
      {%
        \oldchapter[#2]{#3}%
      }%
      {%
        \oldchapter{#3}%
      }%
    }%
  }
% Makes it possible to access the current chapter name.

\newcommand{\firstletter}[1]{\StrChar{#1}{1}}
% Returns the first character of the input.

%%%%%
%%% MAKE IT POSSIBLE TO SWITCH BETWEEN COLOUR AND GRAYSCALE
%%%%%

\newcommand{\colorgray}[2]{#1}
\DeclareOption{grayscale}
{
  \renewcommand{\colorgray}[2]{#2}
}

%%%%%
%%% FOR CHAPTER TITLES, INSERT THE FIRST LETTER AS A BACKGROUND
%%%%%

\DeclareOption{titleletter}
{
  \renewcommand{\chapterlinesformat}[3]%
  {%
    \makebox(0,20)[l]%
    {%
      \hspace{-20pt}%
      \fontsize{60}{6}%
      \selectfont%
      \color{letter-color}%
      \firstletter{\currentchapter}%
    }%
    #3%
  }
}

%%%%%
%%% DISABLE TWOCOLUMN
%%%%%

\DeclareOption{onecolumn}%
{
  \KOMAoptions{twocolumn = false}
}

%%%%%
%%% EXECUTES THE ABOVE OPTIONS (THE ONES THAT ARE USED)
%%%%%

\ProcessOptions\relax


% ------------------------------------------------------------------------------
%   STANDARD VALUES FOR OPTIONS
% ------------------------------------------------------------------------------

\colorgray
{
  \definecolor{letter-color}{RGB}{255, 204, 134}
}{
  \definecolor{letter-color}{gray}{0.8}
}
% light-orange; light grey in grayscale


% ------------------------------------------------------------------------------
%   FURTHER LAYOUT SETTINGS
% ------------------------------------------------------------------------------

\unsettoc{toc}{onecolumn}
\unsettoc{lof}{onecolumn}
\unsettoc{lot}{onecolumn} %layout lot as two columns
% This makes the Table of Contents, List of Figures and List of Tables respect the two-column layout.

\KOMAoption{listof}{totoc}
% Make "listoffigures" and "listoftables" appear in the TOC.

\counterwithout{figure}{chapter}
\counterwithout{table}{chapter}
\renewcommand{\thefigure}{\arabic{figure}}
\renewcommand{\thetable}{\arabic{table}}
% Don’t reset counters for figures and tables between chapters.

\hypersetup{
  colorlinks  = true,
  linkcolor   = \colorgray{orange}{black},
  citecolor   = \colorgray{olive}{black},
  urlcolor    = \colorgray{cyan}{black}
}
% Configure further options of the "hyperref" package: colored links.

\setlist[itemize, 1]{
  nosep,
  label = »
}
\setlist[itemize, 2]{
  nosep,
  label = ›
}
% Global configuration for itemize environments.
% The symbol is » for the first level and › for the second level.

\setlist[description, 2]{
  font = {\normalfont\itshape},
  leftmargin = *
}
% Global configuration for nested description environments.

\newcommand{\marginremark}[1]{%
  \marginpar{%
    \scriptsize%
    \normalcolor%
    \raggedright%
    #1%
  }%
}
% Makes marginpar left-aligned, have the default font colour and a smaller text size.

\setmainfont[
    Ligatures   = TeX,
    Numbers     = OldStyle,
    Contextuals = Alternate
]{Libertinus Serif}
\setsansfont[
    Ligatures   = TeX,
    Numbers     = OldStyle,
    Contextuals = Alternate
]{Libertinus Sans}
\setmonofont[Scale=MatchLowercase]{Inconsolatazi4}
\setmathfont{Libertinus Math}
% Set the text main font to Libertinus Serif via "fontspec".
% Set the sans main font to Libertinus Sans.
% Set the math font to Libertinus Math.
% Enables text figures, fancy "Qu" (contextual alternates).
% Ensures that -- is translated into an endash (TeX ligature).

\newfontfamily{\headingfont}{Libertinus Sans}[
    Ligatures     = TeX,
    Numbers       = OldStyle,
    Contextuals   = Alternate,
    StylisticSet  = 6
]
\addtokomafont{disposition}{\headingfont}
% Fancy version of Libertinus Sans for headings,
% using text figures, fancy ampersand (ss06).

\newfontfamily{\rmlffamily}{Libertinus Serif}
\newfontfamily{\sflffamily}{Libertinus Sans}
% Makes it possible to disable text figures.

\frenchspacing
% No additional space after periods that end sentences.



% ------------------------------------------------------------------------------
%   COMMANDS
% ------------------------------------------------------------------------------

\newcommand{\tblhead}{\textbf}

